const remarkMath = require('remark-math')
const rehypeKatex = require('rehype-katex')

const baseUrl = '/clownbot/'

module.exports = {
  title: 'Clownbot',
  url: 'https://andrewty.gitlab.io',
  baseUrl,
  favicon: 'img/favicon.ico',
  themeConfig: {
    navbar: {
      title: 'Clownbot',
      hideOnScroll: true,
      items: [
        {
          to: 'docs/introduction',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        {
          to: 'notes',
          label: 'Notes',
          position: 'left',
        },
        {
          to: 'playground',
          label: 'Playground',
          position: 'right',
        },
        {
          href: 'https://gitlab.com/andrewty/clownbot',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [],
      copyright: `Copyright © ${new Date().getFullYear()} Andrew. Built with Docusaurus.`,
    },
    prism: {
      theme: require('prism-react-renderer/themes/oceanicNext'),
      additionalLanguages: ['cmake', 'lisp'],
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          remarkPlugins: [remarkMath],
          rehypePlugins: [rehypeKatex],
        },
        blog: {
          path: 'notes',
          routeBasePath: 'notes',
          remarkPlugins: [remarkMath],
          rehypePlugins: [rehypeKatex],
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  stylesheets: [`${baseUrl}css/katex.min.css`],
}
