module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Getting Started',
      items: ['introduction', 'installation'],
    },
    {
      type: 'category',
      label: 'Packages',
      items: [],
    },
  ],
}
