import React from 'react'
import Layout from '@theme/Layout'
import styled from 'styled-components'

const Container = styled.div.attrs({ className: 'container margin-top--lg' })``

const Playground = () => {
  return (
    <Layout title="Playground" noFooter={true}>
      <Container>
        <h1>Playground</h1>
      </Container>
    </Layout>
  )
}

export default Playground
