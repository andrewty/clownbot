(uiop:define-package #:clownbot-math/matrix
  (:use #:cl #:clownbot-math/vector)
  (:export #:matrix #:matrix-rows #:matrix-cols #:matrix-data #:matrix-element-type
           #:mref #:matrixp
           #:make-matrix #:make-identity-matrix #:copy-matrix
           #:matrix= #:m+ #:m- #:.+ #:.- #:.* #:./
           #:transpose))

(in-package #:clownbot-math/matrix)

(defclass matrix ()
  ((rows
    :type integer
    :initarg :rows
    :reader matrix-rows)
   (cols
    :type integer
    :initarg :cols
    :reader matrix-cols)
   (data
    :type array
    :initarg :data
    :accessor matrix-data)))

(defun matrix-element-type (mat)
  "Return the element type of the matrix."
  (array-element-type (matrix-data mat)))

(defun mref (mat row col)
  "Return the element of the matrix by the row and column indexes."
  (with-slots (cols data) mat
    (aref data (+ (* row cols) col))))

(defun (setf mref) (value mat row col)
  "Set the element of the matrix to value by row and column indexes."
  (with-slots (cols data) mat
    (setf (aref data (+ (* row cols) col))
          (coerce value (matrix-element-type mat)))))

(defun matrixp (object)
  "Test whether the argument is a matrix."
  (typep object 'matrix))

(defmethod print-object ((mat matrix) stream)
  (print-unreadable-object (mat stream :type t)
    (with-slots (rows cols) mat
      (format stream " ~D x ~D" rows cols)
      (dotimes (row rows)
        (terpri stream)
        (dotimes (col cols)
          (format stream "~9,3,,,,,'eG " (mref mat row col)))))))

(defun make-matrix (rows cols &key data (element-type 'single-float))
  "Create and return a matrix object."
  (make-instance 'matrix
                 :rows rows
                 :cols cols
                 :data (apply #'make-array (* rows cols) :element-type element-type
                              (when data
                                `(:initial-contents
                                  ,(mapcan (lambda (l)
                                             (mapcar (lambda (x) (coerce x element-type)) l))
                                           data))))))

(defun make-identity-matrix (size &key (element-type 'single-float))
  "Create an identity matrix of the given size."
  (let ((mat (make-matrix size size :element-type element-type)))
    (dotimes (i size mat)
      (setf (mref mat i i) 1))))

(defun copy-matrix (mat)
  "Return a copy of the matrix."
  (with-slots (rows cols data) mat
    (make-instance 'matrix
                   :rows rows
                   :cols cols
                   :data (copy-vector data))))

(defun near-zero? (num &optional (epsilon 1e-7))
  (< (abs num) epsilon))

(defun matrix= (m1 m2 &optional (epsilon 1e-7))
  "Check if two matrices are equal."
  (and (= (matrix-rows m1) (matrix-rows m2))
       (= (matrix-cols m1) (matrix-cols m2))
       (loop for a across (matrix-data m1)
             for b across (matrix-data m2)
             always (near-zero? (- a b) epsilon))))

(defun matrix-map (fn &rest matrices)
  (when matrices
    (let* ((m (first matrices))
           (result (make-matrix (matrix-rows m) (matrix-cols m)
                                :element-type (matrix-element-type m))))
      (apply #'map-into (matrix-data result) fn (mapcar #'matrix-data matrices))
      result)))

(defun m+ (&rest matrices)
  "Add matrices."
  (apply #'matrix-map #'+ matrices))

(defun m- (&rest matrices)
  "Subtract matrices."
  (apply #'matrix-map #'- matrices))

(defmacro def-matrix-scalar-op (name op)
  `(defmethod ,name ((mat matrix) (c number))
     (matrix-map (lambda (val) (,op val c)) mat)))

(def-matrix-scalar-op .+ +)
(def-matrix-scalar-op .- -)
(def-matrix-scalar-op .* *)
(def-matrix-scalar-op ./ /)

(defun transpose (mat)
  "Transpose a matrix."
  (with-slots (rows cols) mat
    (let ((result (make-matrix cols rows :element-type (matrix-element-type mat))))
      (dotimes (row rows result)
        (dotimes (col cols)
          (setf (mref result col row) (mref mat row col)))))))
