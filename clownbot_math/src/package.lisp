(uiop:define-package clownbot-math
  (:use #:cl)
  (:use-reexport #:clownbot-math/vector
                 #:clownbot-math/matrix))
