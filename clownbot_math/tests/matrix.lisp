(uiop:define-package #:clownbot-math-tests/matrix
  (:use #:cl #:lisp-unit #:clownbot-math/matrix)
  (:import-from #:clownbot-testing
                #:lisp-unit->gtest)
  (:export #:test-matrix))

(in-package #:clownbot-math-tests/matrix)

(defun test-matrix ()
  (lisp-unit->gtest :clownbot-math-tests/matrix))

(define-test test-matrix-creation
  (let ((m (make-matrix 2 3 :data '((1 2 3)
                                    (4 5 6)))))
    (assert-equal 2 (matrix-rows m))
    (assert-equal 3 (matrix-cols m))
    (assert-float-equal #(1 2 3 4 5 6) (matrix-data m))))

(define-test test-matrix-equality
  (let ((m1 (make-matrix 2 2 :data '((1 2)
                                     (3 4))))
        (m2 (make-matrix 1 4 :data '((1 2 3 4))))
        (m3 (make-matrix 2 2 :data '((1.001 2)
                                     (3     4)))))
    (assert-false (matrix= m1 m2))
    (assert-false (matrix= m1 m3))
    (assert-true (matrix= m1 m3 1e-2))))

(define-test test-matrix-copy
  (let* ((m1 (make-matrix 2 2 :data '((1 2)
                                      (3 4))))
         (m2 (copy-matrix m1)))
    (assert-true (matrix= m1 m2))))

(define-test test-matrix-addition
  (let ((m1 (make-matrix 2 2 :data '((1 2)
                                     (3 4))))
        (m2 (make-matrix 2 2 :data '((4 3)
                                     (2 1)))))
    (assert-true (matrix= (make-matrix 2 2 :data '((5 5)
                                                   (5 5)))
                          (m+ m1 m2)))
    (assert-true (matrix= (make-matrix 2 2 :data '((6 7)
                                                   (8 9)))
                          (m+ m1 m1 m2)))))

(define-test test-matrix-subtraction
  (let ((m1 (make-matrix 2 2 :data '((4 3)
                                     (2 1))))
        (m2 (make-matrix 2 2 :data '((1 2)
                                     (3 4)))))
    (assert-true (matrix= (make-matrix 2 2 :data '(( 3  1)
                                                   (-1 -3)))
                          (m- m1 m2)))
    (assert-true (matrix= (make-matrix 2 2 :data '(( 2 -1)
                                                   (-4 -7)))
                          (m- m1 m2 m2)))))

(define-test test-matrix-scalar-addition
  (let ((m (make-matrix 2 2 :data '((1 2)
                                    (3 4)))))
    (assert-true (matrix= (make-matrix 2 2 :data '((11 12)
                                                   (13 14)))
                          (.+ m 10)))))

(define-test test-matrix-scalar-subtraction
  (let ((m (make-matrix 2 2 :data '((1 2)
                                    (3 4)))))
    (assert-true (matrix= (make-matrix 2 2 :data '((-9 -8)
                                                   (-7 -6)))
                          (.- m 10)))))

(define-test test-matrix-scalar-multiplication
  (let ((m (make-matrix 2 2 :data '((1 2)
                                    (3 4)))))
    (assert-true (matrix= (make-matrix 2 2 :data '((10 20)
                                                   (30 40)))
                          (.* m 10)))))

(define-test test-matrix-scalar-division
  (let ((m (make-matrix 2 2 :data '((1 2)
                                    (3 4)))))
    (assert-true (matrix= (make-matrix 2 2 :data '((0.1 0.2)
                                                   (0.3 0.4)))
                          (./ m 10)))))

(define-test test-matrix-transpose
  (let ((m (make-matrix 2 3 :data '((1 2 3)
                                    (4 5 6)))))
    (assert-true (matrix= (make-matrix 3 2 :data '((1 4)
                                                   (2 5)
                                                   (3 6)))
                          (transpose m)))))
