(uiop:define-package #:clownbot-math-tests
  (:use #:cl #:lisp-unit
        #:clownbot-math-tests/vector
        #:clownbot-math-tests/matrix)
  (:export #:main))

(in-package #:clownbot-math-tests)

(defun main ()
  (lisp-unit:run-tests :all :clownbot-math-tests/vector)
  (lisp-unit:run-tests :all :clownbot-math-tests/matrix))
